<?php

function factorial(int $numero)
{
    if ($numero == 0) {
        return 1;
    }

    return $numero * factorial($numero - 1);
}

// Ejecución de la función

$numero = 4;
echo "El factorial de $numero es: ".factorial($numero);

$fact = 1;
for ($ind = 1; $ind <= $numero; $ind++) {
    $fact = $ind * $fact;
}

echo "El segudo factorial de $numero es: ".factorial($numero);
