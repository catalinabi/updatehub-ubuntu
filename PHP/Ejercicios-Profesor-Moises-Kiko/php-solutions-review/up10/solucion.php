<?php

function sumaArray(array $numeros)
{
    $suma = 0;

    //OPCIÓN 1 - con un array
    foreach ($numeros as $numero) {
        $suma += $numero;
    }

    return $suma;


    //OPCION 2 - estilo funcional
    /*return array_reduce($numeros, function($sum, $numero) {
        $sum += $numero;
        return $sum;
    });*/

    //OPCION 3 - con una función específica de PHP

    //return array_sum($numeros);
}


$numeros = [25, 1, 12, 45];

echo "La suma es ".sumaArray($numeros);
