<?php

if (!empty($_GET['num'])) {

    $num = $_GET['num'];
    $piramide = [];
    for ($i = 0; $i < $num; $i++) {
        $var = "";
        for ($k = 0; $k < $i; $k++) {
            $var = " ".$var;
        }
        $var = $var."*";
        for ($j = 0; $j < $num - $i - 1; $j++) {
            $var = $var." *";
        }
        array_push($piramide, $var);
    }

    echo "<pre>";
    for ($i = count($piramide) - 1; $i >= 0; $i--) {
        echo $piramide[$i]."\n";
    }
    echo "</pre>";
}
