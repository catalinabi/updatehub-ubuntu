<?php

function mileanizar($texto)
{
    $textoConvertido = str_replace(
        ['que', 'porque', 'gu', 'bu', 'igual', '¡', '', 'c'],
        ['k', 'xq', 'w', 'w', '=', '', '', 'k'],
        strtolower($texto)
    );
    $textoConvertido = eliminarTildes($textoConvertido);
    $textoConvertido = ponerEnMayusculas($textoConvertido);

    return $textoConvertido;
}

function ponerEnMayusculas($texto)
{
    for ($caracter = 0; $caracter < strlen($texto); $caracter++) {
        if (floor(rand(0, 1))) {
            $texto[$caracter] = strtoupper($texto[$caracter]);
        }
    }

    return $texto;
}

function eliminarTildes($texto)
{
    return str_replace(
        ['á', 'é', 'í', 'ó', 'ú'],
        ['a', 'e', 'i', 'o', 'u'],
        $texto
    );
}
