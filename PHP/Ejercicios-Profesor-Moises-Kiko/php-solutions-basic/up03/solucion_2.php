<?php

// solución que optimiza la memoria al leer el fichero
// por linea, así en ficheros muy grandes no tendremos
// problemas que el programa falle por falta de memoria

$fichero = $_FILES['fichero']['tmp_name'];
$palabra = $_POST['palabra'];

$apariciones = 0;

$descriptor = fopen($fichero, 'r');
while (($contenido = fgets($descriptor)) !== false) {
    $count = substr_count(strtolower($contenido), strtolower($palabra));

    $apariciones = $apariciones + $count;
}

fclose($descriptor);

echo "La palabra $palabra se ha encontrado $apariciones veces.";