<?php

$palabra = $_POST['palabra'];
$fichero = $_FILES['fichero']['tmp_name'];

//solución rápida, pero que pone en memoria todo el contenido del fichero.txt
$content = file_get_contents($fichero);

// pasamos tanto el contenido del fichero texto.txt como la palabra
// a minusculas para no tener en cuenta esa diferencia
$apariciones = substr_count(strtolower($content), strtolower($palabra));

echo "El número de veces que aparece la palabra '" . $palabra . "' es: " . $apariciones;
