<?php

$mensajes = [
    'ES' => '¡Bienvenido!',
    'PL' => 'Witamy!',
    'CR' => '¡Pura vida!',
];

$pais = $_GET['pais'];

if (!empty($pais) && isset($mensajes[$pais])) {
    setcookie('mensaje', $mensajes[$pais], time() + 60);
}

header("Location: index.html");
