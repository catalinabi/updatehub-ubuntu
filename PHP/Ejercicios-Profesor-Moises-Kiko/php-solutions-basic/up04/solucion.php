<?php

$basket = [
    'idUsuario' => '34234912',
    'items'     => [
        'Pixel 3'       => 1,
        'Adaptador usb' => 2,
    ],
];

$expiracion = 60 * 60; // 1 hora

setcookie(
    'basket',
    json_encode($basket),
    time() + $expiracion,
    '/',
    'localhost',
    true, // poner a false para que se pueda mandar sin https
    true //Solo http, No puede ser leida por javascript
);

if (empty($_COOKIE['basket'])) {
    echo "No hay galletas todavía";
} else {
    echo $_COOKIE['basket']."<br>";

    $basket = json_decode($_COOKIE['basket'], true);

    echo "Eres el usuario: ".$basket['idUsuario']."<br>";

    foreach ($basket['items'] as $item => $cantidad) {
        echo $item.": ".$cantidad." unidad(es).<br>";
    }
}
