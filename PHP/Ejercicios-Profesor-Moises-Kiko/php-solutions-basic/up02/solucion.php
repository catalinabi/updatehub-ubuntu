<?php

//abrimos el fichero loremp-ipsum.txt en modo lectura
$fd = fopen('texto.txt', 'r');

//inicializamos el contador de lineas
$numLinea = 1;

/*
vamos recorriendo el loremp-ipsum.txt hasta que fgets
devuelva falso, significando eso que hemos llegado al final
del fichero loremp-ipsum.txt y que no hay más lineas que leer.
En la misma sentencia, estamos asignando el contenido de
la línea a la variable $linea
*/
while (($linea = fgets($fd)) !== false) {
    //explode separa un string por la primera variable, devolviendo un
    //array con las partes.
    //count indica el número de elementos que tiene un array
    $numPalabras = count(explode(' ', $linea));
    // Otra alternativa a contar palabras es con la función str_word_count()
    // $numPalabras = str_word_count($linea);
    echo "LINEA $numLinea, PALABRAS: $numPalabras: $linea" . "<br>";

    //incrementamos el contador de lineas
    $numLinea++;
}
