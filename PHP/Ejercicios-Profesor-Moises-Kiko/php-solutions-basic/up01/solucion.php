<?php

$fichero_ = "lorem-ipsum.txt";
$directorio_destino = "./firmado";

//si no existe el directorio 'dist', lo creamos
if (!file_exists($directorio_destino)) {
    mkdir($directorio_destino);
}

copy($fichero, $directorio_destino);

//abrimos el loremp-ipsum.txt en modo escritura y
// nos ponemos al final del mismo
$fd = fopen('./dist/loremp-ipsum.txt', 'a');

//escribimos el contenido
fputs($fd, "AUTOR");
fputs($fd, "Chindasvinto");

fclose($fd);

