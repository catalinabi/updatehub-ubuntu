<?php

class Capitulo extends ElementoMultimedia
{
    private $titulo;
    private $valoracion;
    private $numCapitulo;
    private $thumbnail;

    public function __construct(
        $titulo,
        $duracion,
        $valoracion,
        $numCapitulo,
        $thumbnail
    ) {
        $this->titulo = $titulo;
        $this->valoracion = $valoracion;
        $this->numCapitulo = $numCapitulo;
        $this->thumbnail = $thumbnail;
        parent::__construct($duracion);
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return int
     */
    public function getValoracion()
    {
        return $this->valoracion;
    }

    /**
     * @param int $valoracion
     */
    public function setValoracion($valoracion)
    {
        $this->valoracion = $valoracion;
    }

    /**
     * @return int
     */
    public function getNumCapitulo()
    {
        return $this->numCapitulo;
    }

    /**
     * @param int $numCapitulo
     */
    public function setNumCapitulo($numCapitulo)
    {
        $this->numCapitulo = $numCapitulo;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }
}
