<?php

class Serie
{
    private $nombre;
    private $genero;
    private $temporadas; // un array de objetos Temporada

    public function __construct($nombre, $genero)
    {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->temporadas = [];
    }

    public function addTemporada($temporada)
    {
        //array_push($this->temporadas, $temporada);
        $this->temporadas[] = $temporada;
    }

    public function totalCapitulosPorTemporada($numeroDeTemporada)
    {
        foreach ($this->temporadas as $temporada) {
            if ($temporada->numTemporada == $numeroDeTemporada) {
                return $temporada->getTotalCapitulos();
            }
        }
    }

    public function valoracionMedia()
    {
        $valoracionTotal = 0;
        $total = 0;
        foreach ($this->temporadas as $temporada) {
            foreach ($temporada->getCapitulos() as $capitulo) {
                $valoracionTotal += $capitulo->getValoracion();
            }
            $total += $temporada->getTotalCapitulos();
        }

        return $valoracionTotal / $total;
    }
}
