<?php
require 'Serie.php';
require 'ElementoMultimedia.php';
require 'Capitulo.php';
require 'Temporada.php';


$got = new Serie('Game of Thrones', 'fantasy');
$temporada1 = new Temporada(2008, 1);
$temporada2 = new Temporada(2009, 2);

$capGot1 = new Capitulo('Capitu 1', 60, 9, 1, 'http://got.com/imagen.jpg');
$capGot2 = new Capitulo('Capitu 2', 60, 4, 2, 'http://got.com/imagen2.jpg');
$capGot3 = new Capitulo('Capitu 3', 59, 9, 3, 'http://got.com/imagen3.jpg');
$capGot4 = new Capitulo('Capitu 4', 59, 8, 1, 'http://got.com/imagen4.jpg');

$temporada1->addCapitulo($capGot1);
$temporada1->addCapitulo($capGot2);
$temporada1->addCapitulo($capGot3);

$temporada2->addCapitulo($capGot4);

$got->addTemporada($temporada1);
$got->addTemporada($temporada2);

$numeroCapitulosTemp1 = $got->totalCapitulosPorTemporada(1);
$numeroCapitulosTemp2 = $got->totalCapitulosPorTemporada(2);

$valoracionTotalGoT = $got->valoracionMedia();

echo "TOTAL DE CAPITULOS TEMPORADA 1: ".$temporada1->getTotalCapitulos()."\n";
echo "TOTAL DE CAPITULOS TEMPORADA 2: ".$temporada2->getTotalCapitulos()."\n";
echo "TOTAL DE CAPITULOS SERIE GOT: ".($numeroCapitulosTemp1
        + $numeroCapitulosTemp2)."\n";
echo "VALORACION SERIE GOT: ".$valoracionTotalGoT;
