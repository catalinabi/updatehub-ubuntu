<?php

class Temporada
{
    private $year;
    public $numTemporada;
    private $capitulos = [];

    public function __construct($year, $numTemporada)
    {
        $this->year = $year;
        $this->numTemporada = $numTemporada;
    }

    public function addCapitulo($capitulo)
    {
        $this->capitulos[] = $capitulo;
    }

    public function getTotalCapitulos()
    {
        return count($this->capitulos);
    }

    /**
     * @return Capitulo[]
     */
    public function getCapitulos()
    {
        return $this->capitulos;
    }
}
