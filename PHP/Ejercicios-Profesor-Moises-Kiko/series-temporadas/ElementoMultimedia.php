<?php

class ElementoMultimedia
{
    protected $duracion;

    public function __construct($duracion)
    {
        $this->duracion = $duracion;
    }

    public function getDuracion()
    {
        return $this->duracion;
    }
}
