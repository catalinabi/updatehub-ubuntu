
function validarNombre(){
    let inputNombre = document.getElementById("nombre");
    let errorNombre = document.getElementById("errorNombre");
    if(inputNombre.value.length < 2){
        inputNombre.setAttribute("class","form-input form-input-invalid");
        errorNombre.style.display = "block";
        errorNombre.innerHTML = "El nombre debe tener al menos 2 letras";
        return false;
    }else{
        inputNombre.className="form-input form-input-valid";
        errorNombre.style.display = "none";
        return true;
    }
}

function validarEmail(){
    let inputEmail = document.getElementById("email");
        let posicionPunto = inputEmail.value.lastIndexOf(".");
        
        //Si last index of es > 0 -> no encuentra punto
        if(posicionPunto < 0){
            inputEmail.setAttribute("class","form-input form-input-invalid");
            return false;
        } else {
            let posArroba = inputEmail.value.lastIndexOf("@");
            if(posArroba >= posicionPunto){
                inputEmail.setAttribute("class","form-input form-input-invalid");
                return false;
            }else{
                if(posicionPunto >= inputEmail.value.length -2
                    || posicionPunto < inputEmail.value.length - 5){
                    
                    inputEmail.setAttribute("class","form-input form-input-invalid");
                    return false;
                    
                }else{
                        inputEmail.setAttribute("class","form-input form-input-valid");
                
                        return true;
                }
            }
            
        }
}
/* Validar al envia formulario */ 
function validarAlEnviar(){
    try{//Bloque para vigilar que no haya excepciones
        let resultado = validarNombre() &&  validarEmail();
        return resultado;
        console.log("Resultado es " + resultado);
    } catch(ex) {//Si salta una excepción
        console.log(ex+ "catch");
        return false;
    } 
   
}
/* Validar al envia formulario */ 
function validarLogin(){
    try{//Bloque para vigilar que no haya excepciones
        let resultado = validarEmail();
        return resultado;
        console.log("Resultado es " + resultado);
    } catch(ex) {//Si salta una excepción
        console.log(ex+ "catch");
        return false;
    } 
   
}



