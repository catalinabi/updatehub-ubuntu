<?php


namespace App\Controller;


use App\Entity\Opinion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OpinionController extends AbstractController
{
    /**
     * @Route("/mostrarOpinion", name="mostrarOpinion")
     */
    public function getOpiniones(EntityManagerInterface $em){
        $repo = $em->getRepository(Opinion::class);

        $opiniones = $repo->findAll();

        return $this->render('mostrarOpinion.html.twig',
            ['opiniones' => $opiniones]);
    }

    /**
     * @Route("/insertarOpinion", name="insertarOpinion")
     */
    public function insertarOpinion(Request $request, EntityManagerInterface $em){

        $opinionText = $request->get('opinionText');
        $autor= $request->get('autor');
        $zona = $request->get('zona');
        $ciudad = $request->get('ciudad');

        if ($request->isMethod('POST')) {

            $opinion = new Opinion();
            $opinion->setOpinion($opinionText);
            $opinion->setAutor($autor);
            $opinion->setZona($zona);
            $opinion->setCiudad($ciudad);

            $em->persist($opinion);
            $em->flush();
           
            // Adding 2 success type messages
            $this->addFlash("success", "Gracias por Insertar una opinión");
            
        }
    
       
   

        return $this->render("insertarOpinion.html.twig",
            [
                'opinion' => $opinionText,
                'autor'=>$autor,
                'zona'=>$zona,
                'ciudad'=>$ciudad
            ]);
    }
}