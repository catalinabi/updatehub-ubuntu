<?php


namespace App\Controller;



use App\Entity\UsuarioDemo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RegistroController extends AbstractController
{
    /**
     * @Route("/mostrarRegistros", name="mostrarRegistros")
     */
    public function getDemos(EntityManagerInterface $em){
        $repo = $em->getRepository(UsuarioDemo::class);

        $usuariosRegistrados = $repo->findAll();

        return $this->render('showUsuariosRegistrados.html.twig',
            ['usuariosRegistrados' => $usuariosRegistrados]);
    }

    /**
     * @Route("/registro", name="registro")
     */
    public function insertarRegistro(Request $request, EntityManagerInterface $em){

        $repo = $em->getRepository(UsuarioDemo::class);
        $usuariosDemo = $repo->findAll();
        $urlLogin = "Ir a Login";

        $nombre = $request->get('nombre');
        $email = $request->get('email');
        $contra = $request->get('contraseña');
      
        if ($request->isMethod('POST')) {
            
             //Si la DB esta vacia
             if($usuariosDemo==null){

                $this-> insertarUser($nombre,$email,$contra,$em);
                // Gracias por registrarte
                $this->addFlash("success", "Gracias por Registrarte para acceder a Maleteo");
            }else{
                //Si esta llena
                $emailSiEsta = $repo->findBy(["email"=>$email]);
                if($emailSiEsta!=null){
                    $this->addFlash("error", "El email $email ya existe en la DB");
                }else{
                    $this-> insertarUser($nombre,$email,$contra,$em);
                    $this->addFlash("success", "Acabas de ser registrado para la demo:\n $email con la siguiente contraseña: $contra");
                }
            }
            
             
        }//Request POST        
                    
       

        return $this->render("registro.html.twig",
            [
                'nombre'=>$nombre,
                'email'=>$email,
                'contraseña' => $contra,
                'url'=> $urlLogin
            ]);
    }//insertarRegistro

    function insertarUser($nombre,$email,$contra,EntityManagerInterface $em){
        $usuarioDemo = new UsuarioDemo();
        $usuarioDemo->setNombre($nombre);
        $usuarioDemo->setEmail($email);
        $usuarioDemo->setContra($contra);

        $em->persist($usuarioDemo);
        $em->flush();

    }
}