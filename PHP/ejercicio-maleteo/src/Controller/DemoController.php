<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Demo;
use App\Entity\Opinion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DemoController extends AbstractController
{
    /**
     * @Route("/mostrarDemos", name="mostrarDemos")
     */
    public function getDemos(EntityManagerInterface $em){
        $repo = $em->getRepository(Demo::class);

        $demos = $repo->findAll();

        return $this->render('showDemo.html.twig',
            ['demos' => $demos]);
    }

    
    /**
     * @Route("/maleteo", name="maleteo")
     */
    public function insertarDemo(Request $request, EntityManagerInterface $em){
        $session = $request->getSession();
        $activa = $session->get('activa');
        if (!isset($activa)) {
            //No Existe la Sesion
            return $this->redirectToRoute('login');
             //No existe la sesión
            //return $this->redirectToRoute('login');
            $this->get('session')->getFlashBag()->add(
                'notice-sesion',
                "Debe estar logeado para acceder a Maleteo"
            );

        } else {
           
            

            //Imprmio Opinones
            $repo = $em->getRepository(Opinion::class);
            $opiniones = $repo->findAll();
            $tamanio = (count($opiniones)-1);
            $numeros = range(0,$tamanio);
            shuffle($numeros); 
            $opinionesArray = [];
            
            //Recupero la sesion
            $session = $request->getSession();
            // obtener el valor de un atributo de la sesión
            $usuarioLogeado = $session->get('nombre');


            //Relleno un Array con 3 resultados aleatorios de la SQL Query
            for($i=0;$i<3;$i++){
                $opinionesArray[$i]=$opiniones[$numeros[$i]];
            }
            

            $nombre = $request->get('nombre');
            $email = $request->get('email');
            $ciudad = $request->get('ciudad');
            //Si envia Formulario Demo
            if ($request->isMethod('POST')) {

                $demo = new Demo();
                $demo->setNombre($nombre);
                $demo->setEmail($email);
                $demo->setCiudad($ciudad);

                $em->persist($demo);
                $em->flush();
            
                // Adding  success messages
                $this->addFlash("success", "Gracias por Solicitar una Demo gratis");
                
            } //Post
        }

    
   

        return $this->render("maleteo.html.twig",
            [
                'usuarioLogeado' => $usuarioLogeado,
                'nombre' => $nombre,
                'email'=>$email,
                'ciudad'=>$ciudad,
                'opiniones' => $opinionesArray
            ]);


    }//InsertarDemo


}//DemoController