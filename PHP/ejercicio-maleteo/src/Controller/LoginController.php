<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\UsuarioDemo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{

        /**
     * @Route("/logout", name="logout")
     */
    public function hacerLogout(Request $request, EntityManagerInterface $em){
        
         //Recupero la sesion
         $session = $request->getSession();
         $session->set('activa',false);
         $nombre = $session->get('nombre');
         $urlLogin = "¿Quieres registrarte a la demo como usuario?";

         //$this->addFlash("success", "¡Felicidades! Acaba de logearse $ident en la DEMO como $nombreA");
                    $this->get('session')->getFlashBag()->add(
                        'notice-sesion',
                        "El usuario $nombre acaba de Cerrar sesión"
                    );
                    
        $session->clear();
         
         return $this->redirectToRoute('login');

        
        
    }//hacerLogout
    /**
     * @Route("/login", name="login")
     */
    public function hacerLogin(Request $request, EntityManagerInterface $em){
        
        $repo = $em->getRepository(UsuarioDemo::class);
        $usuariosDemo = $repo->findAll();
        $urlLogin = "¿Quieres registrarte a la demo como usuario?";
        
        
        $email = $request->get('email');
        $contra = $request->get('contraseña');
        $usuarioExiste = $repo->findOneBy(["email" => $email, "contra" => $contra]);
        
        

        if ($request->isMethod('POST')) {
            
            //Si la DB esta vacia
            if($usuariosDemo==null){
                // Debes ir a la pagina de Registro 
                $this->addFlash("success", "Debes ir a la pagina de Registro y registrarte. NO HAY USUARIOS");
            }else{
                
                //Si el usuario existe para ese email y contraseña
                if($usuarioExiste!=null){
                    $nombreA = $usuarioExiste->getNombre();
                    $ident = $usuarioExiste->getId();
                    //Generamos una sesion nueva
                    $session = new Session();
                    $session->start();
                    $session->set('nombre', $nombreA);
                    $session->set('email', $email);
                    $session->set('activa',true);
                    $this->addFlash("success", "¡Felicidades! Acaba de logearse $ident en la DEMO como $nombreA");
                    $this->get('session')->getFlashBag()->add(
                        'notice-sesion',
                        "El usuario $nombreA acaba de Abrir sesión"
                    );
                    return $this->redirectToRoute('maleteo');
                   
                }else{
                    $this->addFlash("error", "Ha insertado mal los credenciales.");
                }
            }
            
             
        }//Request POST        
                    
       

        return $this->render("login.html.twig",
            [
                
                
                'email'=>$email,
                'contraseña' => $contra,
                'url'=> $urlLogin
               
            ]);
    }//hacerLogin

    
}