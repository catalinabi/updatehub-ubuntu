<?php
/*guardo en palabra_buscar la palabra que recibo de 
contar-palabras.html convertida a minuscula*/
$palabra_buscar = strtolower($_POST['palabra']);
/*Guardo en fichero el array files para imprmimirlo en codigo*/ 
$fichero = $_FILES['fichero'];
/*si le doy a ver codigo fuente despues de ejecutar print_r 
vere el array y donde me deja el archivo temporal del quijote:
[tmp_name] => /tmp/phpdBebk4*/
print_r($fichero);
/*Guardo en quijote el contenido del fichero temporal del contenedor
de docket*/ 
$quijote=file_get_contents($fichero['tmp_name']);
//imprimo la palabra a buscar que me ponen en el formulario
echo "La palabra a buscar es $palabra_buscar <br >";
//Salto de linea
echo "<br>";

//Guardo el contenido del fichero en variable contenido
$contenido=strtolower($quijote);
//Funcion substr_count
/*Le paso el contenido del fichero en String y la palabra a buscar y
me devuelve el numero de veces que lo ha encontrado*/ 
$veces=substr_count($contenido,$palabra_buscar);
echo "La palabra $palabra_buscar la ha encontrado $veces veces";