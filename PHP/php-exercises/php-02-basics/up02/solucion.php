<?php
/*Leed un fichero de texto y mostrarlo por pantalla línea a línea,
indicando el número de línea en el que se encuentra.*/ 

// guardo el contenido del archivo text.txt en var archivo
$archivo = fopen('texto.txt','r');
//Guardo en cada linea las lineas del archivo y a su vez las meto en un array
while ($linea = fgets($archivo)) {
    echo $linea.'<br/>';
    $array_con_lineas_archivo[] = $linea;    
    $numlinea++;
}
//cerramos el archivo
fclose($archivo);
//imprimo por codigo el array con las lineas
echo '<pre>';
print_r($array_con_lineas_archivo);
echo '</pre>';