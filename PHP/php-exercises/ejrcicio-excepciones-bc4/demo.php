<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

require 'vendor/autoload.php';

$apiUrl = "https://swapi.co/api/";

$client = new Client();
/*controlamos el error con try and catch*/
try{
    $res = $client->request('GET', $apiUrl . 'people/1');
   


}catch(\ClientException $e){
    echo "Se ha producido un error";
    die;
}
echo "El nombre del personaje con ID 1 es: " . json_decode($res->getBody(), true)['name'] . "\n";

/* Una vez controlado el error creamos un script que 
utilizando la misma API https://swapi.co, 
nos diga los nombres de los planetas 
que salen en la película "La amenaza fantasma". 
Ojo que los nombres están en inglés.*/ 


try{
    $resp = $client->request('GET', $apiUrl . 'films/4/');
    }catch(\ClientException $e){
        echo "Se ha producido un error. No hemos encontrado la pelicula";
        die;
    }

   $urlPlanets= json_decode($resp->getBody(), true)['planets'];
   foreach($urlPlanets as $urlPlanet){
       //$data = [];
       //Sp = new \StarWars\Planet()

   }
vardump($urlPlanets);

