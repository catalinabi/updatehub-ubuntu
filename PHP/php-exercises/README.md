# Ejercicios propuestos PHP

Realizar los ejercicios propuestos en las presentaciones
y registrar los cambios en este repositorio para guardar
las soluciones que se desarrollen.

## Ejecución

Lo más comodo es moverse al directorio donde esta el ejercicio
y desde allí lanzar alguna de las ejecuiciones posibles desde
el comando de linea de comando de PHP (*php-cli*).

### Modos de ejecución de un script PHP

* Ejecutar el ejercicio PHP en modo script de consola...

```bash
php solucion.php
```

Obtendremos por la salida de consola cualquier *echo* o *print* 
que realicemos en el script.

* Ejecutar el ejercicio en modo web...
  * Levantar el servidor web...
```bash
php -S 0.0.0.0:8000 -t .
```
  * Desde un navegador podremos pedir los scripts PHP alojados bajo
  ese directorio...
```bash
http://localhost:8000/solucion.php
```

## Actualización y publicacion de ejercicios

Cada vez que se publiquen más ejercicios estos se añadirán a la
rama **master** por lo que si se está trabajando en otra rama
se deberán traer los cambios de la prinicpal

Actualizar el repositorio...

```bash
git pull origen master
```

 