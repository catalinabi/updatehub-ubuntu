<?php

/*Enunciado:

Escribir un programa que dado un número, cree una pirámide cuya base tiene esa longitud en asteriscos.
Es decir, si el número es 4 el resultado sea:

    *       i=1
   * *      i=3
  * * *     i=3
 * * * *    i=4

Familiarizarse con el tratamiento de cadenas*/ 

/* Para saber cuantos espacios hay, coger la altura y le vas quitando 1
Si N es la altura de la piramide...
Si i es el número de línea (de 1 a N)
Si S es el número de espacios

En cada linea: S = N-i 
                * = i
*/

$altura = $_GET['altura'];
//Pinta linea a linea los espacios
for($i=1;$i<=$altura;$i++){
    for($j=1;$j<=$altura-$i;$j++){

        echo "_";

    }
    for($j=1;$j<=$i; $j++){
        echo "* ";

    }
    echo "<br>";

}
?>