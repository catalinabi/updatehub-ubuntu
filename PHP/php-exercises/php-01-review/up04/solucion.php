<?php

/*Enunciado:

Dado el nombre de 2 ficheros, si la extensión termina en .exe poner en mayúscula
el nombre del fichero, y si termina en .db imprimirlo en minúculas.

Es decir, si los ficheros son pepe.exe y Ana.db, el resultado tiene
que ser PEPE.EXE y ana.db

Objetivo:

Familiarizarse con el tratamiento de strings*/ 



$nombre_fichero= $_GET['fichero'];
echo "<h2>Dado el nombre de 2 ficheros, si la extensión termina en .exe poner en mayúscula
el nombre del fichero, y si termina en .db imprimirlo en minúculas.Es decir, si los ficheros son pepe.exe y Ana.db, el resultado tiene
que ser PEPE.EXE y ana.db</h2>";
echo "<h2>El nombre del fichero es $nombre_fichero</h2>";
cambiarnombre($nombre_fichero);

function cambiarnombre($nombre_fichero){
    
    $tipofichero = explode(".", $nombre_fichero);
    //guardo nombre y tipo
    $nombre=$tipofichero[0];
    $tipo=$tipofichero[1];

    echo ("$nombre y $tipo");
    if($tipo =="exe"){
        $nombre=strtoupper($nombre);
        echo "ES UN EXE -> $nombre.$tipo";
        
    }else{
        $nombre=strtolower($nombre);
        echo "ES UN .DB-> $nombre.$tipo";
    }
}

?>