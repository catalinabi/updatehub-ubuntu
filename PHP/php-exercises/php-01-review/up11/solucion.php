<?php

/*Crear una función que ‘milenialice’ un texto que se le pase, es decir:
    * los ‘que’ los convierte el ‘k’
    porque => xq
    * Pone todo en mayúsculas indistintamente
    * Gu/Bu => w
    * Igual => =
    * Sin apertura de ¡ ni ¿, ni acentuación

Podéis echarle imaginación ;)


Objetivo:

Practicar con funciones cadenas y arrays.
Mover esas funciones a un fichero llamado util.php y hacer llamadas a dichas funciones desde otro fichero diferente. */

convertirAMilenial("los que escriben mucho saben muy poco. Igual si que es cierto. ¡No jodas!");


function convertirAMilenial($frase){
    $texto_antes = array("que", "porque", "Igual","¡","!");
    $texto_despues   = array("k", "pq", "=","","");
    $nueva_frase = str_replace($texto_antes, $texto_despues, $frase);
    echo strtoupper($nueva_frase);

}