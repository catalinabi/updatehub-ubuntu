<?php 
class Pelicula{

    var $titulo;
    var $director;


    function __construct($titulo){
        $this->titulo =$titulo;
        $this->genero = 'comedia';
    }

    public function getDirector(){
        return $this -> director;
    }

    public function setDirector(){
        return $this -> director;
    }
}