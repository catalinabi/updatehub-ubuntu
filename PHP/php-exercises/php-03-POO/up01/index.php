    <?php 
    //Cargo la clase PasswordValidator
    require_once("PasswordValidator.php");

    //Creamos el objeto 
    $validator = new PasswordValidator("123555554_*");
    //Llamamos al metodo isValid del objeto validator que devuelve true or false
    $isValid = $validator->isValid();

    if($isValid){
        echo "La contraseña es válida";
    }else{
        echo "La contraseña NO es válida";
    }