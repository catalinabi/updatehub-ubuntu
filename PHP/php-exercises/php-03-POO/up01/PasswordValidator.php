

<?php 
/* Crear una clase llamada PasswordValidator, 
que tenga un método `isValid()`, 
que reciba una contraseña e indique si cumple 
con las siguiente condiciones:
* Tiene 8 o más caracteres de longitud
* Tiene alguno de los siguientes caracteres ( '.'  ','  '-'  '_'  ';')

Implementar otro método que te diga cuan segura 
es la contraseña. 
(buscar patrones para crear 3 niveles: 
fuerte, medio y débil)

Para probarlo, incluir el fichero de la clase, 
crear un objeto y llamar al método pasándole 
una contraseña
*/
class PasswordValidator{

    var $password;

    function __construct($password){
        //al llamar a this-> password-> hacemos referencia al atributo  var $password de la clase
        //$this->password = $password;-> En esta linea reseteamos el atributo var $password de la clase al parametro que le pasamos desde index.html
        $this->password = $password;
    }


    /* Método propio de la clase isValid()*/ 
    function isValid(){
        $specialCharacters =['.',',','-','_',';'];
        if(strlen($this->password) >= 8){
            foreach($specialCharacters as $sp){
                if(strpos($this->password,$sp)!=false){
                    return true;
                }
            }
          
        }
        return false;

    }//isValid()
}