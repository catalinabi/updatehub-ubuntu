<?php


class Mamifero{
    public $publico;
    protected $protegido;
    private $privado;

    public function metodoPublico(){
        echo "Esto es publico";
    }

    protected function metodoProtegido(){
        echo "Esto es protegido";
    }

    private function metodoPrivado(){
        echo "Esto es privado";
    }
}