<?php 
/* Area Rectangulo: base * Altura */ 
//llamo a la clase de la que heredo "Poligono.php"
require_once "Poligono.php";
//extends significa que la clase Rectangulo.php hereda de la clase Poligono
class Rectangulo extends Poligono
{

    protected $base;
    protected $altura;

    //Constructor-> llamo al constructor desde el index.php y le paso los parametros para inicializarlo
    function __construct(int $base,int $altura){
        $this->base = $base;
        $this->altura = $altura;
    }
    /*Método obligatorio porque esta clase hereda de la clase abstracta Poligono y tienen que tener ambos la misma función
    :int -> significa que le devuevlo un entero;*/
    public function calcularArea():int{
        $area =$this->base*$this->altura;
        return $area;
    }
}