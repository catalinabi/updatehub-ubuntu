    <?php 
    /* Area = base * Altura */ 
    //llamo a la clase de la que heredo "Poligono.php"
    require_once "Poligono.php";
    //extends significa que la clase Circulo.php hereda de la clase Poligono
    //int-> le fuerzo en todos los sitios a que solo le metan enteros
    class Circulo extends Poligono
    {

        protected $radio;

        //Constructor-> llamo al constructor desde el index.php y le paso los parametros para inicializarlo
        function __construct(int $radio){
            $this->radio = $radio;
        }
    //Método obligatorio porque esta clase hereda de la clase abstracta Poligono y tienen que tener ambos la misma función
    //:int -> significa que le devuevlo un entero;
        public function calcularArea():int{
            $area = (3.1416*($this->radio))/2; 
            return $area;
        }
    }