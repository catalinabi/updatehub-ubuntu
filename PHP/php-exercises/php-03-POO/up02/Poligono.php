  
    <?php 
      /* Las clases abstractas no pueden ser instanciadas desde el index.php*/ 
    abstract class Poligono
    {

    /*Método abstracto calcularArea()-> todas las clases que hereden de esta clase tienen que tener este método*/ 
    public abstract function calcularArea();

    }