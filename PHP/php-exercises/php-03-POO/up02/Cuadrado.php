        <?php 
        //llamo a la clase de la que heredo "Poligono.php"
        require_once "Poligono.php";
        /*Area cuadrado-> lado * lado o lado al cuadrado*/ 
        //extends significa que la clase Cuadrado.php hereda de la clase Poligono
        //int-> le fuerzo en todos los sitios a que solo le metan enteros
        class Cuadrado extends Poligono
        {

            protected $lado;
            //Constructor-> llamo al constructor desde el index.php y le paso los parametros para inicializarlo
            function __construct(int $lado){
                $this->lado = $lado;
            }
            //Método obligatorio porque esta clase hereda de la clase abstracta Poligono y tienen que tener ambos la misma función
            //:int -> significa que le devuevlo un entero;
            public function calcularArea():int{
                return $this->lado*$this->lado;

            }
            
        }