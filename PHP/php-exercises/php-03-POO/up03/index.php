<?php

require_once 'Serie.php';
require_once 'Temporada.php';

//Creo una nueva serie Juego de Tronos
$got = new Serie("Juego de Tronos");
$got -> setGenero("Fantasia");

$temporada1got = new Temporada();
$temporada1got.setAño(2005);


$temporada2got = new Temporada();
$temporada2got.setAño(2006);

//Añado Temporadas a la serie Juego de tronos
$got -> addTemporada($temporada1got);
$got -> addTemporada($temporada2got);
