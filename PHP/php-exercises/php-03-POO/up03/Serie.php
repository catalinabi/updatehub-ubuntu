<?php 

class Serie
{


    protected $nombre;
    protected $genero;
    protected $temporadas = [];

     //Constructor-> llamo al constructor desde el index.php y le paso los parametros para inicializarlo
     function __construct($nombre){
        $this->nombre = $nombre;
        //$this->genero = $genero;
    }




    /*   GETTERS AND SETTERS  */ 
    public function getNombre()
    {
        return $this->nombre;
    }

   
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
    

    /* Get genero */
    public function getGenero()
    {
        return $this->genero;
    }

    /* Set Genero */ 
    public function setGenero($genero)
    {
        $this->genero = $genero;

        
    }


    public function getTemporadas()
    {
        return $this->temporadas;
    }


    public function setTemporadas($temporadas)
    {
        $this->temporadas = $temporadas;

        return $this;
    }
    /* Métodos Propios*/
    public function addTemporada(){
        
    }
}//Serie