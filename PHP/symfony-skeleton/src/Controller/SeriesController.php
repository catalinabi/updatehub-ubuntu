<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SeriesController extends AbstractController
{

    /**
     * @Route("/createSerie", name="createSerie")
     */
    public function getPelicula(Request $request)
    {
       
        $titulo = $request ->get('titulo');
        $descripcion = $request ->get('descripcion', 'sin descripción');
        $categoria = $request ->get('categoria');

        if($titulo){
            //Si no existe el .json te crea array vacio
            if(!file_exists('series.json')){
                $series = [];
            }else{
                //Lees el ficheor y json decode a array php
                $seriesJson = file_get_contents('series.json');
                $series = json_decode($seriesJson);
                //
            }

            $series[] = [
                'titulo' => $titulo, 
                'descripcion' => $descripcion,
                'categoria' => $categoria
            ];

           //Lo guardo en el fichero
            file_put_contents('series.json',json_encode($series));

        }
        
        return $this->render('createSerie.html.twig', 
            ['titulo' => $titulo, 
            'descripcion' => $descripcion,
            'categoria' => $categoria
            ]);
        
    }
}
