<?php
/*Añade estos namespaces para poder utilizar Response y Annotation Routes*/
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;

class ControladorWeb extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        return $this->render(
            'homepage.html.twig',
            [
                "nombre" => "kiko",
            ]
        );

        
    }//function

    /**
     * @Route("/contacto", name="contacto")
     */
    public function paginaContacto()
    {
        return $this->render('contacto.html.twig');
        
    }//function
}
