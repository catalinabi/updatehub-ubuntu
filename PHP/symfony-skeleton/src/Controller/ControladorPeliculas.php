<?php
/*Añade estos namespaces para poder utilizar Response y Annotation Routes*/
namespace App\Controller;

use App\Model\Pelicula;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ControladorPeliculas extends AbstractController
{

    /**
     * @Route("/peliculas")
     */
    public function listadoPeliculas()
    {
        return $this->render(
            'peliculas.html.twig',
            [
               'peliculas' => $this->getPeliculas()
            ]
        );

        
    }//function

    public function getPeliculas(): array 
    {
        return [
            new Pelicula('Alien','una pelicula fantastica'),
            new Pelicula('Resurrección','Una peli sobre la teoria de la liberación'),
            new Pelicula('Alf','una pelicula sobre un monstruito')
        ];

    }
   
}
