<?php

/*El namespace viene del achivo autoload.php definido dentro del archivo composer.json
"autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
Entra dentro de la carpeta App/Src y busca la carpeta Controller donde estaran todas 
nuestras clases con controladores*/



/*Añade estos namespaces para poder utilizar Response y Annotation Routes*/
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;

class MiController extends AbstractController
{

    /**
     * @Route("/esPar/{numero}")
     */
    public function comprobadorDePares($numero)
    {

        return $this->render('personalizada.html.twig');
        
    }
}
