<?php
namespace App\Model;

class Pelicula 
{

    private $nombre;
    private $descripcion;

     //Constructor-> llamo al constructor desde el index.php y le paso los parametros para inicializarlo
     function __construct($nombre,$descripcion){
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
    }




    /*   GETTERS AND SETTERS  */ 
    public function getNombre()
    {
        return $this->nombre;
    }

   
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

   
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

}